module mod

go 1.13

require (
	github.com/goburrow/modbus v0.1.0
	github.com/goburrow/serial v0.1.0 // indirect
	gitlab.com/bazhen-paseka/bazhenmod v1.2.0
)
