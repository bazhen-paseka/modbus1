package main

import (
	"encoding/binary"
	"fmt"
	"github.com/goburrow/modbus"
	"math"
	"time"
)

func main() {
	type SetPointsSmartCharging struct {
		serialNumber 					string		//	12 chars
		modelID 						uint16
		firmwareVersion					uint16
		L1voltage 						float64
		L2voltage 						float64
		L3voltage 						float64
		L1current 						float64
		L2current						float64
		L3current 						float64
		chargerPower					float64
		disChargerPower					float64
		minChargePower 					float64
		maxChargePower 					float64
		minDischargePower 				float64
		maxDischargePower 				float64
		batteryCapacity 				float64
		stateOfCharge 					uint16
		chargingStationsState 			uint16
		plugState 						uint16
		maxChargePowerSet 				float64
		maxDischargePowerSet 			float64
		maxFailsafeChargePowerSet 		float64
		maxFailsafeDischargePowerSet 	float64
		failsafeTimeoutSet 				uint16
	}

	var gridX SetPointsSmartCharging

	handler := modbus.NewRTUClientHandler("/dev/ttyUSB0")
	handler.BaudRate 	= 115200
	handler.DataBits 	= 8
	handler.Parity 		= "N"
	handler.StopBits 	= 1
	handler.SlaveId 	= 1
	handler.Timeout 	= 30 * time.Minute

	var errConnect = handler.Connect()
	if errConnect == nil {
		fmt.Printf("Connect status-Ok\r\n" )
	} else {
		fmt.Printf("Connect ERROR: %v\r\n", errConnect )
	}

	//defer handler.Close()

	var client = modbus.NewClient(handler)
	var result, errRead = client.ReadInputRegisters(0, 80)
	//	80	- because that's the size of our SetPointsSmartCharging structure.
	if errRead == nil {
		fmt.Printf("ReadInputRegisters-Ok\r\n" )
	} else {
		fmt.Printf("ReadInputRegisters ERROR: %v\r\n", errRead )
	}

	var nextInputReg = 0
	gridX.serialNumber,					nextInputReg = stringFromReg ( result, nextInputReg)
	gridX.modelID,  					nextInputReg = uint16fromReg ( result, nextInputReg)
	gridX.firmwareVersion,				nextInputReg = uint16fromReg ( result, nextInputReg)
	gridX.L1voltage, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.L2voltage, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.L3voltage, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.L1current, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.L2current, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.L3current, 					nextInputReg = float64fromReg( result, nextInputReg)
	gridX.chargerPower, 				nextInputReg = float64fromReg( result, nextInputReg)
	gridX.disChargerPower, 				nextInputReg = float64fromReg( result, nextInputReg)
	gridX.minChargePower, 				nextInputReg = float64fromReg( result, nextInputReg)
	gridX.maxChargePower, 				nextInputReg = float64fromReg( result, nextInputReg)
	gridX.minDischargePower, 			nextInputReg = float64fromReg( result, nextInputReg)
	gridX.maxDischargePower,			nextInputReg = float64fromReg( result, nextInputReg)
	gridX.batteryCapacity, 				nextInputReg = float64fromReg( result, nextInputReg)
	gridX.stateOfCharge,				nextInputReg = uint16fromReg ( result, nextInputReg)
	gridX.chargingStationsState,		nextInputReg = uint16fromReg ( result, nextInputReg)
	gridX.plugState,					nextInputReg = uint16fromReg ( result, nextInputReg)
	gridX.maxChargePowerSet,			nextInputReg = float64fromReg( result, nextInputReg)
	gridX.maxDischargePowerSet,			nextInputReg = float64fromReg( result, nextInputReg)
	gridX.maxFailsafeChargePowerSet,	nextInputReg = float64fromReg( result, nextInputReg)
	gridX.maxFailsafeDischargePowerSet,	nextInputReg = float64fromReg( result, nextInputReg)
	gridX.failsafeTimeoutSet,			nextInputReg = uint16fromReg ( result, nextInputReg)

	fmt.Printf("serialNumber:\t\t\t%v\r\n",				gridX.serialNumber )
	fmt.Printf("model_ID:\t\t\t%v\r\n",					gridX.modelID )
	fmt.Printf("firmwareVersion:\t\t%v\r\n",				gridX.firmwareVersion)
	fmt.Printf("L1voltage:\t\t\t%v\r\n",					gridX.L1voltage )
	fmt.Printf("L2voltage:\t\t\t%v\r\n",					gridX.L2voltage )
	fmt.Printf("L3voltage:\t\t\t%v\r\n",					gridX.L3voltage )
	fmt.Printf("L1current:\t\t\t%v\r\n",					gridX.L1current )
	fmt.Printf("L2current:\t\t\t%v\r\n",					gridX.L2current )
	fmt.Printf("L3current:\t\t\t%v\r\n",					gridX.L3current )
	fmt.Printf("chargerPower:\t\t\t%v\r\n",				gridX.chargerPower )
	fmt.Printf("disChargerPower:\t\t%v\r\n",				gridX.disChargerPower )
	fmt.Printf("minChargePower:\t\t\t%v\r\n",			gridX.minChargePower )
	fmt.Printf("maxChargePower:\t\t\t%v\r\n",			gridX.maxChargePower )
	fmt.Printf("minDischargePower:\t\t%v\r\n",			gridX.minDischargePower )
	fmt.Printf("maxDischargePower:\t\t%v\r\n",			gridX.maxDischargePower )
	fmt.Printf("batteryCapacity:\t\t%v\r\n",				gridX.batteryCapacity )
	fmt.Printf("stateOfCharge:\t\t\t%v\r\n",				gridX.stateOfCharge )
	fmt.Printf("chargingStationsState:\t\t%v\r\n",		gridX.chargingStationsState )
	fmt.Printf("plugState:\t\t\t%v\r\n",					gridX.plugState )
	fmt.Printf("maxChargePowerSet:\t\t%v\r\n",			gridX.maxChargePowerSet )
	fmt.Printf("maxDischargePowerSet:\t\t%v\r\n",		gridX.maxDischargePowerSet )
	fmt.Printf("maxFailsafeChargePowerSet:\t%v\r\n",		gridX.maxFailsafeChargePowerSet )
	fmt.Printf("maxFailsafeDischargePowerSet:\t%v\r\n",	gridX.maxFailsafeDischargePowerSet )
	fmt.Printf("failsafeTimeoutSet:\t\t%v\r\n",			gridX.failsafeTimeoutSet )

	fmt.Printf("\r\nNextInputRegister:\t\t%v\r\n",			nextInputReg )
}

func Float64fromBytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}

func uint16fromReg( grid []byte, position int) (uint16, int) {
	// 2 - because uint16 has 2 byte
	res := (uint16(grid[  position + 0 ]))<<8 + uint16(grid[ position + 1 ])
	nextInputReg := position + 2
	return res, nextInputReg
}

func float64fromReg( grid []byte, position int) (float64, int) {
	// 8 - because float64 have 8 byte
	res := Float64fromBytes( grid[ position:(position+8) ] )
	nextInputReg := position + 8
	return res, nextInputReg
}

func stringFromReg( grid []byte, position int) (string, int) {
	// 12 - because our "serialNumber" has 12 chars (byte)
	res := string(grid[position:(position + 12)])
	nextInputReg := position + 12
	return res, nextInputReg
}